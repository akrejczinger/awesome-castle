--[[
    Cesious Awesome WM theme
    Created by Culinax
--]]

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
naughty = require("naughty")
local menubar = require("menubar")
-- Vicious
local vicious = require("vicious")
local helpers = require("vicious.helpers")
-- Lain
local lain = require("lain")
-- freedesktop.org
local freedesktop = require('freedesktop')

-- notification window references
error_window = nil

-- redshift for changing hue with the movement of the sun
redshift = {}
redshift.redshift = '/usr/bin/redshift'
redshift.options = ' -c ~/.config/redshift.conf'
redshift.enabled = true

-- function for padding text to a given length
function lpad(str, len, char)
    if char == nil then char = ' ' end
    return str .. string.rep(char, len - #str)
end

-- Draw a progress bar.
-- percent(int): percentage of bar filled
-- bar_length(int): progress bar length in characters
-- bar_color(hex string)='#00FF00': color of filled portion of the bar
-- end_marker(char)='>': char to draw at the end of the filled portion
function progress_bar(percent, bar_length, bar_color, end_marker)
    percent = tonumber(percent)
    bar_color = bar_color or '#00FF00'
    end_marker = end_marker or '>'
    if bar_length == nil  or bar_length == "" then
        error_window = naughty.notify({ preset = naughty.config.presets.critical,
                                        title = "Bad value for bar_length: " .. bar_length,
                                        replaces_id = error_window}).id
        return "ERROR"
    end
    if percent == nil or percent == "" then
        return "NO SOUND DEVICE"
    end
    bar = ''
    current_length = 0
    filled = math.floor(bar_length * percent / 100)
    for i=1, filled do
        bar = bar .. '='
        current_length = current_length + 1
    end
    bar = bar .. end_marker
    bar = markup(bar_color, bar)
    while current_length < bar_length do
        bar = bar .. '-'
        current_length = current_length + 1
    end
    return bar
end

function redshift.toggle()
    if redshift.enabled then
        awful.spawn.with_shell('pkill redshift && redshift -x')
        redshift.enabled = false
        shell_notify('Redshift is disabled.')
    else
        awful.spawn.with_shell(redshift.redshift .. redshift.options)
        redshift.enabled = true
        shell_notify('Redshift is enabled.')
    end
end

local mousecoords = {x=0, y=10000} -- bottom left corner
local function move_mouse(x_co, y_co)
    mouse.coords({x=x_co, y=y_co})
end

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    error_window = naughty.notify({ preset = naughty.config.presets.critical,
                                    title = "Oops, there were errors during startup!",
                                    text = awesome.startup_errors,
                                    replaces_id = error_window}).id
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        error_window = naughty.notify({ preset = naughty.config.presets.critical,
                                        title = "Oops, an error happened!",
                                        text = err,
                                        replaces_id = error_window}).id
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init(awful.util.getdir("config") .. "/themes/cesious/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
-- terminal = "urxvt"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
--  awful.layout.suit.floating,
    awful.layout.suit.tile,
--  awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
--  awful.layout.suit.tile.top,
    awful.layout.suit.fair,
--  awful.layout.suit.fair.horizontal,
--  awful.layout.suit.spiral,
--  awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
--  awful.layout.suit.max.fullscreen,
--  awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {
  names  = { "1", "2", "3", "4", "5"},
  layout = { layouts[4], layouts[1], layouts[1], layouts[1], layouts[1] }
}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome", "help" },
   { "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua" },
   { "edit theme", editor_cmd .. " " .. awful.util.getdir("config") .. "/themes/cesious/theme.lua" },
   { "restart", awesome.restart },
   { "quit", "oblogout" },
}
mymainmenu = freedesktop.menu.build({
    before = {
        { "Awesome", myawesomemenu, beautiful.awesome_icon },
    },
    after = {
        { "Open terminal", terminal },
    },
})
mymainmenu.theme.width = 150
mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })
-- }}}

-- {{{ wibar
markup      = lain.util.markup
darkblue    = theme.bg_focus
blue        = "#9EBABA"
red         = "#EB8F8F"

-- Separators
spacer = wibox.widget.textbox(" ")
separator = wibox.widget.textbox(markup(darkblue, "|"))

-- Create a textclock widget
mytextclock = wibox.widget.textclock("%b %d. %I:%M %p")
-- Show calendar when hovering over mytextclock
lain.widget.cal({
   attach_to = {mytextclock},
   cal = "/usr/bin/cal",
   followtag = true,
   notification_preset = {
       font = "Monospace 11",
       fg   = theme.fg_normal,
       bg   = theme.bg_normal
   }
})

-- CPU indicator
cpu_icon = wibox.widget.imagebox()
cpu_icon:set_resize(false)
cpu_icon:set_image(beautiful.widget_cpu)
cpu = wibox.widget.textbox()
vicious.register(cpu, vicious.widgets.cpu,
    function (widget, args)
        return lpad(args[1] .. '%', 3)
    end, 2)
cpu:buttons(awful.util.table.join(
    awful.button({}, 1, function()
        awful.spawn.with_shell(terminal .. " -e htop")
    end),
    awful.button({}, 3, function()
        awful.spawn.with_shell(terminal .. " -e htop")
    end)
))

-- Memory indicator
mem_icon = wibox.widget.imagebox()
mem_icon:set_resize(false)
mem_icon:set_image(beautiful.widget_mem)
mem = wibox.widget.textbox()
vicious.register(mem, vicious.widgets.mem,
    function (widget, args)
        return lpad(args[1] .. '%', 3)
    end, 5)
mem:buttons(awful.util.table.join(
    awful.button({}, 1, function()
        awful.spawn.with_shell(terminal .. " -e htop")
    end),
    awful.button({}, 3, function()
        awful.spawn.with_shell(terminal .. " -e htop")
    end)
))

-- CPU temperature
thermal_icon = wibox.widget.imagebox()
thermal_icon:set_resize(true)
thermal_icon:set_image(beautiful.widget_thermal)
cputemp = wibox.widget.textbox()
vicious.register(cputemp, vicious.widgets.hwmontemp, "$1°C", 10, {"coretemp"})

-- -- Touchpad indicator
-- local touchpad_enabled = false
-- local touchtoggle = function()
--     if touchpad_enabled then
--         touchpad_enabled = false
--         touchwidget:set_image(awful.util.getdir("config").."/icons/touchoff.png")
--         awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/touchpad.sh Off")
--     else
--         touchpad_enabled = true
--         touchwidget:set_image(awful.util.getdir("config").."/icons/touchon.png")
--         awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/touchpad.sh On")
--     end
-- end
-- touchwidget = wibox.widget.imagebox()
-- if touchpad_enabled then
--     touchwidget:set_image(awful.util.getdir("config").."/icons/touchon.png")
--     awful.spawn.with_shell("xinput enable 'AlpsPS/2 ALPS GlidePoint'")
-- else
--     touchwidget:set_image(awful.util.getdir("config").."/icons/touchoff.png")
--     awful.spawn.with_shell("xinput disable 'AlpsPS/2 ALPS GlidePoint'")
-- end
-- touchwidget:buttons(awful.util.table.join(
--     awful.button({}, 1, touchtoggle),
--     awful.button({}, 3, touchtoggle)
-- ))

-- PulseAudio
vol_icon = wibox.widget.imagebox()
vol_icon:set_resize(false)
-- PulseAudio volume
local vol = lain.widget.pulse {
    timeout = 1,
    settings = function()
        if volume_now.muted == "yes" then
            level = markup(red, lpad(volume_now.left .. "M", 3))
            vol_icon:set_image(beautiful.widget_sound_mute)
            bar_color = '#FF0000'
            end_marker = 'X'
        else
            level = lpad(volume_now.left .. "%", 3)
            vol_icon:set_image(beautiful.widget_sound_max)
            bar_color = '#00FF00'
            end_marker = '>'
        end
        bar_length = 24
        bar = progress_bar(volume_now.left, bar_length, bar_color, end_marker)
        widget:set_markup(bar .. " " .. level)
    end
}
-- Volume Mouse -- LeftClick=1 RightClick=3 ScrollUp=4 ScrollDown=5
vol.widget:buttons(awful.util.table.join(
    awful.button({}, 1, function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh mute")
        vol.update()
    end),
    awful.button({}, 3, function ()
        awful.spawn.with_shell("pavucontrol")
        vol.update()
    end),
    awful.button({}, 4, function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh up 2")
        vol.update()
    end),
    awful.button({}, 5, function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh down 2")
        vol.update()
    end)
))

-- Keyboard layout switcher
local kbd_layout = lain.widget.contrib.kbdlayout {
    layouts = { { layout = "us", variant = "colemak" },
                { layout = "us" } },
    settings = function()
        if kbdlayout_now.variant then
            -- enable my own xmodmap if colemak is selected
            if kbdlayout_now.variant == "colemak" then
                awful.spawn.with_shell("xmodmap ~/.Xmodmap")
            end

            widget:set_text(string.format("%s/%s", kbdlayout_now.layout,
            kbdlayout_now.variant))
        else
            widget:set_text(kbdlayout_now.layout)
        end
    end
}

shell_notify_id = nil
shell_notify_bar = function(title, bar_percent, bar_width, bar_color, end_marker)
    local bar = progress_bar(bar_percent, bar_width, bar_color, end_marker)

    shell_notify_id = naughty.notify({preset = naughty.config.presets.normal,
                                      title = title,
                                      text = tostring(bar_percent) .. ' ' .. bar,
                                      timeout = 2,
                                      replaces_id = shell_notify_id}).id
end

shell_notify = function(title, text)
    shell_notify_id = naughty.notify({preset = naughty.config.presets.normal,
                                      title = title,
                                      text = text,
                                      timeout = 2,
                                      replaces_id = shell_notify_id}).id
end

-- Battery
bat = wibox.widget.textbox()
bat_icon = wibox.widget.imagebox()
bat_icon:set_resize(false)
bat_icon:set_image(beautiful.widget_bat_full)
plug_status = wibox.widget.textbox()
vicious.register(bat, vicious.widgets.bat,
    function (widget, args)
        local plugged = args[1]
        local charge = math.min(args[2], 99)
        local perc = lpad(tostring(charge), 2)

        -- Update battery icon
        if charge <= 10 then
            bat_icon:set_image(beautiful.widget_bat_low)
            perc = markup(red, perc)
        elseif charge < 50 then
            bat_icon:set_image(beautiful.widget_bat_low)
        elseif charge < 99 then
            bat_icon:set_image(beautiful.widget_bat_full)
        elseif charge >= 99 then
            bat_icon:set_image(beautiful.widget_bat_full)
        end

        -- Update plug icon
        if plugged == '-' then
            plug_status:set_text('-')
        else
            plug_status:set_text('+')
        end

        -- Update battery percentage indicator
        return perc .. '%'
    end, 10, "BAT0")

-- MPD widget
mpd_icon = wibox.widget.imagebox()
mpd_icon:set_resize(false)
mpd_widget = lain.widget.mpd({
    timeout=0.5,
    settings = function()
        if mpd_now.state == "play" then
            mpd_icon:set_image(beautiful.widget_mpd_play)
        elseif mpd_now.state == "pause" then
            mpd_icon:set_image(beautiful.widget_mpd_pause)
        elseif mpd_now.state == "stop" then
            mpd_icon:set_image(beautiful.widget_mpd_stop)
        end
    end
})
mpdbuttons = awful.util.table.join(
    awful.button({}, 1, function()
        awful.spawn.with_shell("mpc toggle")
    end),
    awful.button({}, 2, function()
        awful.spawn.with_shell("mpc stop")
    end),
    awful.button({}, 3, function()
        awful.spawn.with_shell(terminal .. " -e ncmpcpp")
    end)
)
mpd_icon:buttons(mpdbuttons)

-- MPD current song tooltip
mpd_tooltip = awful.tooltip({
    objects={mpd_icon},
    timer_function = function()
        local info = awful.util.pread("mpc status")
        local lines = {}
        local num_lines = 0
        for line in info:gmatch("[^\n]+") do
            num_lines = num_lines + 1
            lines[num_lines] = line
        end

        if num_lines <= 1 then
            return '\n Stopped. \n'
        end

        local artist_title = ' ' .. lines[1]
        local progress = lines[2]:match("%b()")
        local progress_int = tonumber(progress:match("%d+"))

        local bar_length = math.max(20, artist_title:len())
        song_text = '\n' .. artist_title .. '\n'
        song_text = helpers.escape(song_text) .. progress_bar(progress_int, bar_length) .. '\n'
        return song_text
    end,
})

-- Create a wibar for each screen and add it
mywibar = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      c:tags()[1]:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibar
    mywibar[s] = awful.wibar({ position = "top", height = tostring(beautiful.menu_height), screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(spacer)
    left_layout:add(mytaglist[s])
    left_layout:add(mylayoutbox[s])
    left_layout:add(spacer)

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(spacer)
    -- Note: X11 limitation makes it impossible to add systray to multiple screens
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    -- right_layout:add(touchwidget)
    right_layout:add(separator)
    right_layout:add(kbd_layout.widget)
    right_layout:add(separator)
    right_layout:add(vol_icon)
    right_layout:add(vol.widget)
    right_layout:add(mpd_icon)
    right_layout:add(separator)
    right_layout:add(cpu_icon)
    right_layout:add(cpu)
    right_layout:add(separator)
    right_layout:add(mem_icon)
    right_layout:add(mem)
    right_layout:add(separator)
    right_layout:add(thermal_icon)
    right_layout:add(cputemp)
    right_layout:add(separator)
    right_layout:add(bat_icon)
    right_layout:add(bat)
    right_layout:add(plug_status)
    right_layout:add(separator)
    right_layout:add(mytextclock)

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibar[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    -- awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    -- awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "l",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "l", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "l", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "t", function () awful.spawn.with_shell(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", function () awful.spawn.with_shell(
        awful.util.getdir("config") .. "/scripts/shutdown.lua") end),
    awful.key({ modkey,           }, "v", function ()
        awful.spawn.with_shell("keepass2 --auto-type") end),
    awful.key({ modkey,           }, "k",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.client.incwfact( 0.05)  end),
    awful.key({ modkey, "Shift"   }, "k",     function () awful.client.incwfact(-0.05)  end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "k",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),
    awful.key({ modkey,           }, "r", redshift.toggle),

    -- awful.key({ modkey }, "x",
    --           function ()
    --               awful.prompt.run({ prompt = "Run Lua code: " },
    --               mypromptbox[mouse.screen].widget,
    --               awful.util.eval, nil,
    --               awful.util.getdir("cache") .. "/history_eval")
    --           end),

    -- Menubar
    awful.key({ modkey }, "p", function () awful.spawn.with_shell(
        "dmenu_run -h '" .. beautiful.menu_height .. "' -i -nb '" .. beautiful.bg_normal
        .. "' -nf '" .. beautiful.fg_normal
        .. "' -sb '" .. beautiful.bg_focus
        .. "' -sf '" .. beautiful.fg_focus
        .. "' -fn 'Monoid:pixelsize=13'")
    end),

    -- Switch to specific layout
    awful.key({ modkey, "Control" }, "f", function () awful.layout.set(awful.layout.suit.floating) end),
    awful.key({ modkey, "Control" }, "t", function () awful.layout.set(awful.layout.suit.tile) end),
    awful.key({ modkey, "Control" }, "b", function () awful.layout.set(awful.layout.suit.tile.bottom) end),
    awful.key({ modkey, "Control" }, "s", function () awful.layout.set(awful.layout.suit.fair) end),
    awful.key({ modkey, "Control" }, "m", function () awful.layout.set(awful.layout.suit.max) end),
    awful.key({ modkey,           }, ";", function () move_mouse(mousecoords.x, mousecoords.y) end),
    awful.key({ modkey,           }, "Right", function () move_mouse(20, 40) end),
    awful.key({ modkey,           }, "Up", function () move_mouse(400, 400) end),
    -- awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey, "Control" }, "g", function () awful.layout.set(awful.layout.suit.max.fullscreen) end),

    -- Volume control (Script which uses 'volnoti' as notification)
    awful.key({ }, "XF86AudioRaiseVolume", function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh up 2")
        vol.update() end),
    awful.key({ }, "XF86AudioLowerVolume", function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh down 2")
        vol.update() end),
    awful.key({ }, "XF86AudioMute",        function ()
        awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/volume.sh mute")
        vol.update() end),
    awful.key({ }, "XF86AudioNext",        function ()
        awful.spawn.with_shell("mpc next")
        end),
    awful.key({ }, "XF86AudioPrev",        function ()
        awful.spawn.with_shell("mpc prev")
        end),
    awful.key({ }, "XF86AudioPlay",        function ()
        awful.spawn.with_shell("mpc toggle")
        end),

    -- Brightness control (Script which uses 'volnoti' as notification) SYNTAX: up/down
    awful.key({ modkey, "Shift" }, ".", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py up") end),
    awful.key({ modkey, "Shift" }, "+", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py up") end),
    awful.key({ modkey, "Shift" }, ",", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py down") end),
    awful.key({ modkey, "Shift" }, "-", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py down") end),
    -- awful.key({ }, "XF86MonBrightnessUp", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py up") end),
    -- awful.key({ }, "XF86MonBrightnessDown", function () awful.spawn.with_shell(awful.util.getdir("config") .. "/scripts/brightness.py down") end),
    awful.key({ }, "XF86TouchpadToggle", touchtoggle),
    -- Screen lock
    -- awful.key({ modkey, "Shift" }, "l", function () awful.spawn.with_shell("dm-tool lock") end),

    -- Screenshot
    awful.key({ }, "Print", function () awful.spawn.with_shell("scrot -e 'mv $f ~/Pictures/'") end),
    awful.key({ "Shift" }, "Print", function () awful.spawn.with_shell("scrot -s -e 'mv $f $HOME/Pictures/'") end),

    -- Applications
    awful.key({ modkey }, "c", function () awful.spawn.with_shell("chromium") end)
    -- awful.key({ modkey }, "s", function () awful.spawn.with_shell("steam") end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,           }, "m",      function (c) c.maximized = not c.maximized    end),
    awful.key({ modkey,           }, "q",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      function () awful.screen.focus_relative(1)    end),
    awful.key({ modkey, "Control" }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = screen.tags[i]
                      if client.focus and tag then
                          client.focus:move_to_tag(tag)
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = screen.tags[i]
                      if client.focus and tag then
                          awful.client.toggletag(tag)
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     size_hints_honor = false, -- Remove gaps between terminals
                     callback = awful.client.setslave } }, -- Open new clients as slave instead of master
    { rule = { class = "Microsoft Teams" },
      properties = { maximized_vertical = false,
                     maximized_horizontal = false }
    },
    { rule_any = { class = {"pinentry", "Oblogout", "Galculator"},
                   name = {"Event Tester"},
                   instance = {"plugin-container", "exe"} },
                   properties = { floating = true } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    -- Open programs on active screen
    awful.client.movetoscreen(c, mouse.screen)

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Autostart applications.
awful.spawn.with_shell(redshift.redshift .. redshift.options)
awful.spawn.with_shell("setxkbmap us -variant colemak &")
awful.spawn.with_shell("sleep 1 && xmodmap ~/.Xmodmap &")
awful.spawn.with_shell("nm-applet &")
awful.spawn.with_shell("keepassxc &")
-- awful.spawn.with_shell("dropbox &")
awful.spawn.with_shell("odrive &")
awful.spawn.with_shell("bash ~/.profile &")
awful.spawn.with_shell("xscreensaver -no-splash &")
awful.spawn.with_shell("conky &")
