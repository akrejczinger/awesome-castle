#!/usr/bin/env lua5.3
-- TODO: consider using python instead

local function isempty(str)
    return str == nil or str == ''
end

options = {
    ["shutdown"] = [[systemctl poweroff]],
    ["restart"] = [[systemctl reboot]],
    ["logout"] =[[echo "awesome.quit()" | awesome-client]],
    ["sleep"] = [[systemctl suspend]],
    ["lock"] = [[xscreensaver-command -lock]]
}

opts = {}
for name,cmd in pairs(options) do
    table.insert(opts, name)
end

-- TODO: use beautiful theme
normbg = "#000000"
normfg = "#ffffff"
focusbg = "#285577"
focusfg = "#ffffff"

optstring = table.concat(opts, '\n')
option = io.popen("echo '"..optstring.."' | dmenu -h '18' -i -nb '"..normbg.."' -nf '"..normfg.."' -sb '"..focusbg.."' -sf '"..focusfg.."' -fn 'Terminus:size=9'")
option = option:read("*a")
cmd = nil
if not isempty(option) then
    while option:sub(-1) == '\n' do
        option = option:sub(1,-2)
    end
    print(option)
    cmd =options[option]
    print(cmd)
end

if cmd then
    os.execute(cmd)
end
