#!/usr/bin/env zsh
USAGE="Usage: touchpad.sh On|Off"

declare -i ID
ID=`xinput list | grep -Eio '(touchpad|glidepoint)\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}'`

case $argv[1] in
    (On)
        TITLE="'Touchpad on '"
        read -d '' TEXT <<"EOF"
[[
/-------\\
|       |
|  On.  |
|       |
\\-------/
]]
EOF
        xinput enable $ID
        ;;
    (Off)
        TITLE="'Touchpad off'"
        read -d '' TEXT <<"EOF"
[[
/-------\\
|  \\ /  |
|   X   |
|  / \\  |
\\-------/
]]
EOF
        xinput disable $ID
        ;;
    (*)
        echo $USAGE
        ;;
esac

echo $TITLE
echo "shell_notify($TITLE, $TEXT)" | awesome-client
